package ru.agilix.blackjackkata.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Dealer {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean canHit(ArrayList<Card> cards) {
        if (cards.size() >= 5) {
            return false;
        };

        Map<String, List<Card>> cardMap = cards
                .stream()
                .collect(Collectors.groupingBy(Card::getKey));

        int sum = cards.stream().mapToInt(Card::getValue).sum();

        if (cardMap.containsKey("A") && cardMap.get("A").size() > 1) {
            sum -= 10 * (cardMap.get("A").size() - 1);
        }

        return sum < 17 ;
    }
}
