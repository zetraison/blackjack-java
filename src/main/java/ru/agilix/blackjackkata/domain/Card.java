package ru.agilix.blackjackkata.domain;

import java.util.HashMap;
import java.util.Map;

public class Card {

    private final int value;
    private Map<String, Integer> values = new HashMap<>();

    private final String card;

    public int getValue() {
        return value;
    }

    public String getKey() {
        return card;
    }

    public Card(String card) {
        this.card = card;
        initValues();
        this.value = calculateValue(card);
    }

    private void initValues() {
        values.put("A",11);
        values.put("K",10);
        values.put("Q",10);
        values.put("J",10);
        values.put("10",10);
        values.put("9",9);
        values.put("8",8);
        values.put("7",7);
        values.put("6",6);
        values.put("5",5);
        values.put("4",4);
        values.put("3",3);
        values.put("2",2);
    }

    private int calculateValue(String cart) {
        return values.get(cart);
    }

    @Override
    public String toString() {
        return "["+card+"]";
    }
}
